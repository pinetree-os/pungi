#!/bin/sh
export LC_ALL=C
LABEL=$1
# Remove the label from arguments. It gets special treatment. Other arguments
# to the script are passed to pungi-koji directly.
shift
CONFIG="fedora-final.conf"
TARGET_DIR="/mnt/koji/compose/26"
#OLD_COMPOSES_DIR="--old-composes=/mnt/fedora_koji/compose/f23 --old-composes=$TARGET_DIR"
NIGHTLY=""
SKIP_PHASES="--skip-phase=productimg"
DEST=$(pwd)
DATE=$(date "+%Y%m%d")
COMPSFILE="comps-f26.xml"
TMPDIR=`mktemp -d /tmp/fedoraRC.$DATE.XXXX`
# uncomment and edit for resuming a failed compose
#COMPOSE_ID="Fedora-23-20150530.n.0"
pushd $TMPDIR
git clone https://pagure.io/fedora-comps.git && {
    pushd fedora-comps
    make "${COMPSFILE}"
    cp "${COMPSFILE}" $DEST/
    popd
}
popd
CMD="pungi-koji --notification-script=/usr/bin/pungi-fedmsg-notification --notification-script=pungi-wait-for-signed-ostree-handler --config=$CONFIG --old-composes=$TARGET_DIR $OLD_COMPOSES_DIR $NIGHTLY $SKIP_PHASES --label=$LABEL"
if [ -z "$COMPOSE_ID" ]; then
    CMD="$CMD --target-dir=$TARGET_DIR"
else
    CMD="$CMD --debug-mode --compose-dir=$TARGET_DIR/$COMPOSE_ID"
fi
time $CMD "$@"
