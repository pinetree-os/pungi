#!/bin/sh
#set -x
export LC_ALL=C
LABEL=$1
# Remove the label from arguments. It gets special treatment. Other arguments
# to the script are passed to pungi-koji directly.
shift
CONFIG="fedora-cloud.conf"
TARGET_DIR="/mnt/koji/compose/cloud"
#OLD_COMPOSES_DIR="--old-composes=/mnt/fedora_koji/compose/f23 --old-composes=$TARGET_DIR"
NIGHTLY=""
DEST=$(pwd)
DATE=$(date "+%Y%m%d")
# the Pungi 'shortname', which we will include in fedmsgs for disambiguation
SHORT="Fedora-Cloud"
RELEASE="35"
RELEASE_TITLE="35"
COMPSFILE="comps-f35.xml"
TMPDIR=`mktemp -d /tmp/$RELEASE.$DATE.XXXX`
TOMAIL="devel@lists.fedoraproject.org test@lists.fedoraproject.org"
FROM="Fedora Branched Report <rawhide@fedoraproject.org>"
RSYNCPREFIX="sudo -u ftpsync"
OLDCOMPOSE_ID=$(cat $TARGET_DIR/latest-$SHORT-$RELEASE_TITLE/COMPOSE_ID)
# uncomment and edit for resuming a failed compose
#COMPOSE_ID="Fedora-23-20150530.n.0"
# assume a releng dir is a git checkout of the releng repo
# if it does not exist clone it
if [ -d releng ]; then
    pushd releng
    git pull --rebase
    popd
else
    git clone https://pagure.io/releng.git
fi
# Set up our fedmsg function, using the releng repo definition
fedmsg_json_start=$(printf '{"log": "start", "branch": "%s", "arch": "%s", "short": "%s"}' "$RELEASE" "$ARCH" "$SHORT")
FEDMSG_MODNAME="compose"
FEDMSG_CERTPREFIX="releng"
. ./releng/scripts/fedmsg-functions.sh
# Announce that we are starting, even though we don't know the compose_id yet..
send_fedmsg "${fedmsg_json_start}" ${RELEASE} start
CMD="pungi-koji --notification-script=/usr/bin/pungi-fedmsg-notification --config=$CONFIG --old-composes=$TARGET_DIR $OLD_COMPOSES_DIR $NIGHTLY --label=$LABEL"
if [ -z "$COMPOSE_ID" ]; then
    CMD="$CMD --target-dir=$TARGET_DIR"
else
    CMD="$CMD --debug-mode --compose-dir=$TARGET_DIR/$COMPOSE_ID"
fi
time $CMD "$@"
if [ "$?" != "0" ]; then
    exit 1
fi

NEWCOMPOSE_ID=$(cat $TARGET_DIR/latest-$SHORT-$RELEASE_TITLE/COMPOSE_ID)
SHORTCOMPOSE_ID=$(echo $NEWCOMPOSE_ID|sed -e 's|Fedora-.*-||g')

# Set this to use later for a few items include depcheck
DESTDIR=$TARGET_DIR/$NEWCOMPOSE_ID
# Public URL the compose will wind up at, we put it in fedmsgs
LOCATION=$(echo $DESTDIR|sed -e 's,/mnt/koji,https://kojipkgs.fedoraproject.org,g')
# Update fedmsg template
fedmsg_json_start=$(printf '{"log": "start", "branch": "%s", "arch": "%s", "short": "%s", "compose_id": "%s", "location": "%s"}' "$RELEASE" "$ARCH" "$SHORT" "$NEWCOMPOSE_ID", "$LOCATION")
fedmsg_json_done=$(printf '{"log": "done", "branch": "%s", "arch": "%s", "short": "%s", "compose_id": "%s", "location": "%s"}' "$RELEASE" "$ARCH" "$SHORT" "$NEWCOMPOSE_ID" "$LOCATION")

$RSYNCPREFIX mkdir -p $DESTDIR
# Tell interested persons that the rsync is starting (zomg!)
#send_fedmsg "${fedmsg_json_start}" ${RELEASE} rsync.start
#for dir in CloudImage metadata ;
#  do
#    $RSYNCPREFIX rsync -avhH $DESTDIR/compose/$dir/ /pub/alt/atomic/testing/$SHORTCOMPOSE_ID/$dir/ ;
#  done
# Tell interested persons that the rsync is done.
#send_fedmsg "${fedmsg_json_done}" ${RELEASE} rsync.complete
# Tell everyone by fedmsg about the compose
send_fedmsg "${fedmsg_json_done}" ${RELEASE} complete
# Tell everyone by email about the compose
SUBJECT='Fedora '$RELEASE' compose report: '$SHORTCOMPOSE_ID' changes'
#for tomail in $TOMAIL ; do
#    cat $DESTDIR/logs/*verbose $DESTDIR/logs/depcheck | \
#         mutt -e "set from=\"$FROM\"" -e 'set envelope_from=yes' -s "$SUBJECT" $tomail
#done

find $TARGET_DIR  -xdev -depth -maxdepth 2 -mtime +14  -exec rm -rf {} \;
